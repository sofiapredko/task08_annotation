public class MyClass {

    @MyAnnotation(name = "George")
    private String name;
    private int age;
    private String profession;
    private int valueExperience = 2;

    public MyClass() {
        this.name = name;
    }

    public MyClass(String name, int age, String profession) {
        this.name = name;
        this.age = age;
        this.profession = profession;
    }

    @Override
    public String toString() {
        return "Name: " + name + ", age: " + age + ", profession: " + profession;
    }

    @MyAnnotation(name ="Yura", age = 18, profession = "programmer")
    void fun1()
    {
        System.out.println("Test method 1");
        System.out.println(name + " " + age + " " + profession);
    }

    public int getValueExperience() {
        return valueExperience;
    }

    public void setValueExperience(int valueExperience) {
        this.valueExperience = valueExperience;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
