import java.lang.reflect.*;

public class Main {
    public static void main(String[] args) throws Exception {
        MyClass testObject = new MyClass("Anya", 21, "manager");
        System.out.println("Object of class: " + testObject.toString());

        System.out.println("--------------------------------------");
        Class cls1 = testObject.getClass();
        System.out.println("The name of class is " + cls1.getName());

        int mods = cls1.getModifiers();
        System.out.print("Class access modifier: ");
        if (Modifier.isPublic(mods)) {
            System.out.println("public");
        }
        if (Modifier.isAbstract(mods)) {
            System.out.println("abstract");
        }
        if (Modifier.isFinal(mods)) {
            System.out.println("final");
        }

        Constructor[] constructors = cls1.getDeclaredConstructors();
        System.out.println("Constructors: ");
        for (Constructor constructor : constructors) {
            Class[] paramTypes = constructor.getParameterTypes();
            System.out.println(constructor.getName());
            for (Class paramType : paramTypes) {
                System.out.print(paramType.getName() + " ");
            }
            System.out.println();
        }

        System.out.println("--------------------------------------");
        System.out.println("Fields of class: ");
        Field[] fields = cls1.getDeclaredFields();
        for (Field f : fields) {
            Class fieldType = f.getType();
            System.out.print("Field name: " + f.getName() + "; Field Type: " + fieldType.getName());
            MyAnnotation anno = f.getAnnotation(MyAnnotation.class);
            if (anno != null) {
                System.out.println("Dafault value for \"name\" in Annotation: " + anno.name());
                System.out.println("Dafault value for \"age\" in Annotation: " + anno.age());
                System.out.println("Dafault value for \"profession\" in Annotation: " + anno.profession());
            }
            System.out.println();
        }

        System.out.println("--------------------------------------");
        System.out.println("Declared methods of class are : ");
        Method[] methods = cls1.getDeclaredMethods();

        for (Method method : methods)
            System.out.println(method.getName());


        System.out.println("--------------------------------------");
        System.out.println("Invoke methods and change class fields");
        Method methodCall2 = cls1.getDeclaredMethod("toString");
        String info = (String) methodCall2.invoke(testObject);
        System.out.println(info);

        try {
            Method methodcall1 = cls1.getDeclaredMethod("setAge", int.class);
            methodcall1.invoke(testObject, 19);

            methodCall2.invoke(testObject);

            Method mCall2 = cls1.getDeclaredMethod("setValueExperience", int.class);
            mCall2.invoke(testObject, 9);
            System.out.println(testObject.toString());

            Field field1 = cls1.getDeclaredField("name");
            field1.setAccessible(true);
            field1.set(testObject, "Marina");
            System.out.println(testObject.toString());

            Method methodCall3 = cls1.getDeclaredMethod("setProfession", String.class);
            methodCall3.setAccessible(true);
            methodCall3.invoke(testObject, "programmer");
            System.out.println(testObject.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
