import java.lang.annotation.*;

@Inherited
@Documented
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PACKAGE})
@Retention(RetentionPolicy.RUNTIME)
public @interface  MyAnnotation {
    int age() default 19;
    String  name() default "Unknown";
    String  profession() default "";
}
